/*
<tl;dr>
  C code to translate NTP time into a hue for LEDs; time is represented by that hue

  Basically, the program ([`color-clock-wifi-only.ino`](color-clock-wifi-only.ino)) will:

  1. get the the current time via WiFi (using the Network Time Protocol (NTP))
	- default time zone is UTC+1 (e.g. Berlin/Germany)
	- winter/summer time will be adjusted automatically
  2. translate this time into a specific spectral color
	- essentially, 12h are divided into 256 colors (≈2¾min per color)
	- the resulting hue is adjusted to human vision (human eyes are more sensitive to green/yellow than to red/blue (see [this chart](https://en.wikipedia.org/wiki/Color_vision#/media/File:Eyesensitivity.svg) and the [FastLED Wiki](https://github.com/FastLED/FastLED/wiki/FastLED-HSV-Colors)))
	- color transitions are "hard" (yet), there's no fading; however the steps are small enough that
  3. send this color value to LEDs
  4. wait 16875ms

the code has been tested with an ESP32 (DOIT ESP32 DEVKIT V1)

<libraries used>
  - WiFi.h
  - ezTime.h v.0.8.3
  - FastLED.h v3.5.0
*/

#include <ezTime.h>
#include <WiFi.h>
#include "FastLED.h"

// set values

#define DATA_PIN 4
#define NUM_LEDS 8

CRGB leds[NUM_LEDS]; //block of memory that will be used for storing and manipulating the led data (`leds` array)

const char* ssid       = "WIFI-NAME";
const char* password   = "WIFI-PW";

double led_time = 0;
double led_sec = 0;
uint16_t led_hue = 0;
uint16_t led_hue_debug = 0;
uint32_t hue_delay = (60*60*12)*1000/256; //in ms for 256 colours

// Initialize WiFi
void initWiFi() {
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
  Serial.print("Connecting to WiFi ..");
  while (WiFi.status() != WL_CONNECTED) {
    Serial.print('.');
    delay(1000);
  }
  Serial.println(WiFi.localIP());
}

// define default time zone
Timezone Germany;

void setup() {
//WiFi Setup

  Serial.begin(9600);
  initWiFi();
  waitForSync();

//Time Setup
  Germany.setLocation("Europe/Berlin");
  Germany.setDefault();

  Serial.println("UTC: " + UTC.dateTime());
  Serial.println("Berlin time: " + Germany.dateTime());
  Serial.println("adjusting time for LEDs...");

//LED Setup
  FastLED.addLeds<WS2812, DATA_PIN, GRB>(leds, NUM_LEDS).setCorrection(TypicalSMD5050); //load LED data
  FastLED.setBrightness(255); //set global brightness, applied regardless of what color(s) are shown on the LEDs

//  leds[0] = CRGB::White; FastLED.show(); //LED test

// set LED hue
  led_time = Germany.tzTime(); // returns the current time in seconds since midnight Jan 1st 1970 in the timezone specified https://github.com/ropg/ezTime#tztime
  
  tmElements_t tm;
  breakTime(led_time, tm);
  
  led_sec = tm.Hour*(60*60)+tm.Minute*60+tm.Second;
  led_hue = led_sec*256/(60*60*12);
  
  for(int i = 0; i<NUM_LEDS; i++){ 
  leds[i].setHue(led_hue);
  }
  FastLED.show();
  
// print (mainly for debugging)
  Serial.println("LED time is: ");
  Serial.print(tm.Hour); Serial.print(tm.Minute); //prints the time (broken down from led_time), for details see: https://github.com/ropg/ezTime#breaktime
  Serial.println(" ");
  Serial.println("which equals this much seconds (since 0:00h today):");
  Serial.print (led_sec);
  Serial.println(" ");
  Serial.println("…which is a hue of: ");
  Serial.print(led_hue);
  Serial.println(" ");Serial.println(" ");

  //disconnect WiFi as it's no longer needed
  WiFi.disconnect(true);
  WiFi.mode(WIFI_OFF);
  if (WiFi.status() != WL_CONNECTED) {Serial.println("WiFi DISCONNECTED (no longer needed)");}
  Serial.println(" ");Serial.println(" ");  Serial.println(" ");Serial.println(" ");

  delay(hue_delay);
}

void loop() {
  led_hue++; // increment LED hue after hue_delay milliseconds
  
  for(int i = 0; i<NUM_LEDS; i++){ 
  leds[i].setHue(led_hue);
  }
  FastLED.show();

// log for debugging hue...... (uncomment if needed)
/*
  led_time = Germany.tzTime(); // returns the current time in seconds since midnight Jan 1st 1970 in the timezone specified https://github.com/ropg/ezTime#tztime
  
  tmElements_t tm;
  breakTime(led_time, tm);
  
  led_sec = tm.Hour*(60*60)+tm.Minute*60+tm.Second;
  led_hue_debug = led_sec*256/(60*60*12);
  Serial.println("LED time is: ");
  Serial.print(tm.Hour); Serial.print(tm.Minute); //prints the time (broken down from led_time), for details see: https://github.com/ropg/ezTime#breaktime
  Serial.println(" ");
  Serial.println("which equals this much seconds (since 0:00h today):");
  Serial.print (led_sec);
  Serial.println(" ");
  Serial.println("automatically incremented hue is now: ");
  Serial.print(led_hue);
  Serial.println(" ");
  Serial.println("debug_hue is: ");
  Serial.print(led_hue_debug);
  Serial.println(" ");Serial.println(" ");
  Serial.println(" ");
  Serial.println("hue_delay (really) is: ");
  Serial.print(hue_delay);
*/
  delay(hue_delay);
}
